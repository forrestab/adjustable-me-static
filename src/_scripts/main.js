var Main = (function(Testimonial) {

    var init = function() {
        Testimonial.loadRandom();
    };

    return {
        init: init
    };

})(Testimonial);
