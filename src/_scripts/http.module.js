// https://github.com/mdn/js-examples/blob/master/promises-test/index.html
// https://stackoverflow.com/a/30008115

var Http = (function() {

    var isSuccess = function(status) {
        return status >= 200 && status < 300;
    };

    var ajax = function(method, url, responseType) {
        return new Promise(function(resolve, reject) {
            var request = new XMLHttpRequest();

            request.open(method, url);
            request.responseType = responseType;
            request.onload = function onLoad() {
                if(isSuccess(this.status)) {
                    resolve(request.response);
                } else {
                    reject(request.statusText);
                }
            };
            request.onerror = function onError() {
                reject(request.statusText);
            };
            request.send();
        });
    };

    var isPromiseSupported = function() {
        return typeof Promise !== "undefined";
    };

    var get = function(url) {
        return ajax("GET", url, "json");
    };

    return {
        isPromiseSupported: isPromiseSupported,
        get: get
    };
    
})();
