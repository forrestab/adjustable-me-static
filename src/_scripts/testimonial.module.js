var Testimonial = (function(Http, Css, Utils) {

    var changeTestimony = function(testimonial) {
        var quote = document.getElementById("quote");
        
        document.getElementById("testimony").innerHTML = testimonial.testimony;
        document.getElementById("attestant").innerHTML = testimonial.attestant;
        Css.removeClass(quote, "c-testimonial__quote--loading");
    };

    var loadRandom = function() {
        if(Http.isPromiseSupported()) {
            Http.get("/testimonials.json").then(function(response) {
                changeTestimony(response[Utils.randomize(response.length)]);
            });
        }
    };

    return {
        loadRandom: loadRandom
    };    

})(Http, Css, Utils);
