var Css = (function() {

    var buildClassNameRegEx = function(className) {
        return new RegExp("(\\s|^)" + className + "(\\s|$)");
    };

    var hasClass = function(element, className) {
        return !!element.className.match(buildClassNameRegEx(className));
    };

    var addClass = function(element, className) {
        if(!hasClass(element, className)) {
            element.className += " " + className;
        }
    };

    var removeClass = function(element, className) {
        if(hasClass(element, className)) {
            element.className = element.className.replace(buildClassNameRegEx(className), "");
        }
    };

    return {
        addClass: addClass,
        hasClass: hasClass,
        removeClass: removeClass
    };

})();
