var Utils = (function() {

    var randomize = function(length) {
        return Math.floor(Math.random() * length);
    };

    return {
        randomize: randomize
    };

})();
