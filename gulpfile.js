const gulp = require("gulp");
const util = require("gulp-util");
const browserSync = require("browser-sync");
const child = require("child_process");
const sass = require("gulp-sass");
const concat = require("gulp-concat");

gulp.task("jekyll:build", ["sass:build", "js:build"], (done) => {
    browserSync.notify("");

    return child.spawn("jekyll.bat", [
        "build",
        "--incremental",
        //"--drafts"
    ], { stdio: "inherit" }).on("close", done);
});

gulp.task("jekyll:rebuild", ["jekyll:build"], () => {
    browserSync.reload();
});

gulp.task("sass:build", () => {
    return gulp.src("./src/_sass/main.scss")
        .pipe(sass({
            includePaths: [
                "./node_modules/bootstrap/scss",
                "./src/_sass"
            ]
        }))
        .pipe(browserSync.reload({ stream: true }))
        .pipe(gulp.dest("./src/assets"));
});

gulp.task("js:build", () => {
    return gulp.src([
        "./src/_scripts/css.module.js",
        "./src/_scripts/http.module.js",
        "./src/_scripts/utils.module.js",
        "./src/_scripts/testimonial.module.js",
        "./src/_scripts/main.js",
    ])
    .pipe(concat("main.min.js"))
    .pipe(gulp.dest("./src/assets"));
});

gulp.task("serve", ["jekyll:build"], () => {
    browserSync.init({
        server: "public",
        port: 4000
    });

    gulp.watch([
        "./src/**/*.md",
        "./src/**/*.html",
        "./src/_data/*.yml",
        "./src/_sass/*.scss",
        "./src/_scripts/*.js"
    ], ["jekyll:rebuild"]);
});

gulp.task("default", ["serve"]);
